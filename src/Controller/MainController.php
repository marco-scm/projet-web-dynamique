<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/category", name="category")
     */
    public function category()
    {
        return $this->render('main/category.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/connexion", name="login")
     */
    public function connexion()
    {
        return $this->render('main/connexion.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }


    /*
     * @Route("/inscription", name="inscription")
     */
    /*
    public function inscription()
    {
        return $this->render('main/inscription.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }
    */

    /**
     * @Route("/profile/compte", name="compte")
     */
    public function compte()
    {
        return $this->render('main/compte.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/produit", name="produit")
     */
    public function produit()
    {
        return $this->render('main/produit.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    /**
     * @Route("/recherche", name="recherche")
     */
    public function recherche()
    {
        return $this->render('main/rechercher.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }


    /**
     * page logout
     *
     *@Route("/logout", name="logout")
     */
    public function logout(){

       $this->addFlash('success', 'Vous êtes déconnecté, à bientôt!');  
       return $this->render('main/index.html.twig');  

        
    }

}

